package com.hiking.testvideo;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;
import android.view.SurfaceView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Test encode
 *
 * @author Created by hiking on 2017/9/22.
 */
class HikingEncoder {

	private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding

	private static final String TAG = "HikingEncoder";

	static void encode(String inputPath, String outputPath, SurfaceView surfaceView) {
		MediaCodec encoder = null;
		MediaCodec decoder = null;
		MediaExtractor extractor = null;
		try {
			File inputFile = new File(inputPath);
			if (!inputFile.canRead()) {
				throw new FileNotFoundException("Unable to read " + inputFile);
			}

			extractor = new MediaExtractor();
			extractor.setDataSource(inputFile.toString());

			String mimeType = null;
			MediaFormat originalFormat = null;
			for (int i = 0; i < extractor.getTrackCount(); ++i) {
				originalFormat = extractor.getTrackFormat(i);
				mimeType = originalFormat.getString(MediaFormat.KEY_MIME);
				if (mimeType.startsWith("video/")) {
					// Must select the track we are going to get data by readSampleData()
					extractor.selectTrack(i);
					break;
				}
			}
			if (mimeType == null) {
				throw new RuntimeException("No video track found");
			}

			int originalWidth = originalFormat.getInteger(MediaFormat.KEY_WIDTH);
			int originalHeight = originalFormat.getInteger(MediaFormat.KEY_HEIGHT);
			Log.d(TAG, "Original video size is " + originalWidth + "x" + originalHeight);

			MediaFormat newFormat = MediaFormat
					.createVideoFormat(MIME_TYPE, originalWidth / 4 / 4 * 4,
							originalHeight / 4 / 4 * 4);

			decoder = MediaCodec.createDecoderByType(mimeType);
			decoder.configure(originalFormat, surfaceView.getHolder().getSurface(), null, 0);
			decoder.start();

			// Encode
			encoder = MediaCodec.createEncoderByType(MIME_TYPE);
			encoder.configure(newFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
			encoder.start();
			FileOutputStream outputStream = new FileOutputStream(outputPath);

			ByteBuffer[] inputBuffers = decoder.getInputBuffers();
			ByteBuffer[] outputBuffers = decoder.getOutputBuffers();
			MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
			int timeoutUs = 1000000; // 1 second timeout
			boolean eos = false;
			while (!eos) {
				int decoderInputBufferIndex = decoder.dequeueInputBuffer(timeoutUs);
				if (decoderInputBufferIndex >= 0) {
					ByteBuffer inputBuffer = inputBuffers[decoderInputBufferIndex];
					int sampleSize = extractor.readSampleData(inputBuffer, 0);
					if (sampleSize > 0) {
						// Video data is valid,send input buffer to MediaCodec for decode
						decoder.queueInputBuffer(decoderInputBufferIndex, 0, sampleSize,
								extractor.getSampleTime(), 0);
						extractor.advance();
					} else {
						// End-Of-Stream (EOS)
						decoder.queueInputBuffer(decoderInputBufferIndex, 0, 0, 0,
								MediaCodec.BUFFER_FLAG_END_OF_STREAM);
						eos = true;
					}
				}

				int timeout = 10000;
				int outIndex = decoder.dequeueOutputBuffer(info, timeout);
				ByteBuffer buffer = outputBuffers[outIndex];

				int encoderInputBufferIndex = encoder.dequeueInputBuffer(timeout);
//				encoder.queueInputBuffer(encoderInputBufferIndex, 0, frameData.length, ptsUsec, 0);
				decoder.releaseOutputBuffer(outIndex, true);

				// All decoded frames have been rendered, we can stop playing now
				if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
					Log.d(TAG, "BUFFER_FLAG_END_OF_STREAM");
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (extractor != null) {
				extractor.release();
			}
			if (encoder != null) {
				encoder.stop();
				encoder.release();
			}
			if (decoder != null) {
				decoder.stop();
				decoder.release();
			}
		}
	}

	/**
	 * Returns the first codec capable of encoding the specified MIME type, or null if no
	 * match was found.
	 */
	private static MediaCodecInfo selectCodec(String mimeType) {
		int numCodecs = MediaCodecList.getCodecCount();
		for (int i = 0; i < numCodecs; i++) {
			MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);

			if (!codecInfo.isEncoder()) {
				continue;
			}

			String[] types = codecInfo.getSupportedTypes();
			for (int j = 0; j < types.length; j++) {
				if (types[j].equalsIgnoreCase(mimeType)) {
					return codecInfo;
				}
			}
		}
		return null;
	}
}
