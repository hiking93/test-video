package com.hiking.testvideo;

import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;

import net.ypresto.androidtranscoder.format.MediaFormatStrategy;

/**
 * Transcoder strategy.
 *
 * @author Created by hiking on 2017/10/11.
 */
class HikingFormatStrategy implements MediaFormatStrategy {

	private static final boolean TRANSCODE_LOWER_VIDEOS = false;
	private static final String TAG = "HikingFormatStrategy";

	private static final int[] AVAILABLE_LONGER_LENGTHS = {1280, 960, 720};
	private static final int TARGET_SHORTER_LENGTH = 720;
	private static final int VIDEO_BITRATE = 3000 * 1000; // Instagram bitrate 3Mb/s

	@Override
	public MediaFormat createVideoOutputFormat(MediaFormat inputFormat) {
		int width = inputFormat.getInteger(MediaFormat.KEY_WIDTH);
		int height = inputFormat.getInteger(MediaFormat.KEY_HEIGHT);
		int shorterLength, outWidth, outHeight;
		if (width >= height) {
			// Horizontal video
			shorterLength = height;
			outWidth = getLongerLength(width, height);
			outHeight = TARGET_SHORTER_LENGTH;
		} else {
			// Vertical video
			shorterLength = width;
			outWidth = TARGET_SHORTER_LENGTH;
			outHeight = getLongerLength(height, width);
		}
		if (shorterLength <= TARGET_SHORTER_LENGTH && !TRANSCODE_LOWER_VIDEOS) {
			Log.d(TAG, "This video is less than or equals to " + TARGET_SHORTER_LENGTH +
					"p, keep original. (" + width + "x" + height + ")");
			return null;
		} else {
			Log.d(TAG, "Transcode video to " + outWidth + "x" + outHeight);
		}
		MediaFormat format = MediaFormat.createVideoFormat("video/avc", outWidth, outHeight);
		format.setInteger(MediaFormat.KEY_BIT_RATE, VIDEO_BITRATE);
		format.setInteger(MediaFormat.KEY_FRAME_RATE, 30);
		format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 3);
		format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
				MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
		return format;
	}

	private int getLongerLength(int sourceLongerLength, int sourceShorterLength) {
		float targetRatio = sourceLongerLength / (float) sourceShorterLength;

		int bestLongerLength = AVAILABLE_LONGER_LENGTHS[0];
		float bestBias = AVAILABLE_LONGER_LENGTHS[0] / (float) TARGET_SHORTER_LENGTH;
		for (int aLongerLength : AVAILABLE_LONGER_LENGTHS) {
			float ratio = aLongerLength / (float) TARGET_SHORTER_LENGTH;
			float bias = Math.abs(ratio - targetRatio);
			if (bias < bestBias) {
				bestBias = bias;
				bestLongerLength = aLongerLength;
			}
		}

		return bestLongerLength;
	}

	@Override
	public MediaFormat createAudioOutputFormat(MediaFormat inputFormat) {
		return null; // Use format from source.
	}
}