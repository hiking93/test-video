package com.hiking.testvideo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import net.ypresto.androidtranscoder.MediaTranscoder;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

import life.knowledge4.videotrimmer.utils.FileUtils;

public class MainActivity extends AppCompatActivity {

	private static final int REQUEST_VIDEO_CAPTURE = 101;
	private static final int REQUEST_VIDEO_SELECTION = 102;
	private static final int REQUEST_VIDEO_TRIM_K4L = 104;

	private static final boolean ENABLE_TRIM = false;
	private static final boolean USE_TRANSCODER_LIBRARY = true;
	private Button mCaptureButton;
	private Button mChooseButton;
	private Button mChooseCustomButton;
	private SurfaceView mSurfaceView;
	private VideoView mVideoView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mCaptureButton = findViewById(R.id.button_capture);
		mChooseButton = findViewById(R.id.button_choose);
		mSurfaceView = findViewById(R.id.surface);
		mVideoView = findViewById(R.id.video);

		if (ContextCompat.checkSelfPermission(MainActivity.this,
				Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(MainActivity.this,
					new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1234);
		}
		mCaptureButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
				startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
			}
		});
		mChooseButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("video/*");
				intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 100 * 1024 * 1024);
				intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 50 * 1024 * 1024);
				startActivityForResult(intent, REQUEST_VIDEO_SELECTION);
			}
		});

		mChooseCustomButton = findViewById(R.id.button_choose_custom);
		mChooseCustomButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				startActivity(new Intent(MainActivity.this, VideoChooserActivity.class));
			}
		});
	}

	@SuppressLint("SetTextI18n")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		TextView textView = findViewById(R.id.text_debug);
		textView.setText("Request code: " + requestCode);
		textView.append("\nResult code: " + resultCode);
		textView.append("\nData: " + data);

		Uri uri = data == null ? null : data.getData();
		textView.append("\nUri: " + uri);
		if (uri != null) {
			if (!ENABLE_TRIM) {
				if (USE_TRANSCODER_LIBRARY) {
					transcode(uri);
				} else {
					final String path = FileUtils.getPath(this, uri);
					new Thread(new Runnable() {

						@Override
						public void run() {
							HikingEncoder.encode(path,
									Environment.getExternalStorageDirectory().getPath() +
											"/fuck.mp4", mSurfaceView);
						}
					}).start();
				}
			} else {
				switch (requestCode) {
					case REQUEST_VIDEO_CAPTURE:
					case REQUEST_VIDEO_SELECTION:
						trim(uri);
						break;
					case REQUEST_VIDEO_TRIM_K4L:
						MediaController mediaController = new MediaController(this);
						mVideoView.setMediaController(mediaController);
						mVideoView.setVideoURI(uri);
						mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

							@Override
							public void onPrepared(MediaPlayer mediaPlayer) {
								mVideoView.start();
							}
						});
						break;
				}
			}
		}
	}

	private void transcode(Uri uri) {
		try {
			final ParcelFileDescriptor parcelFileDescriptor =
					getContentResolver().openFileDescriptor(uri, "r");
			if (parcelFileDescriptor == null) {
				throw new FileNotFoundException("Null file descriptor.");
			}
			final FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();

			final ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setTitle("Transcoding...");
			progressDialog.setMax(100);
			progressDialog.setCancelable(false);
			progressDialog.show();

			final File outputFile =
					new File(Environment.getExternalStorageDirectory(), "transcode.mp4");
			MediaTranscoder.getInstance()
					.transcodeVideo(fileDescriptor, outputFile.getAbsolutePath(),
							new HikingFormatStrategy(), new MediaTranscoder.Listener() {

								@Override
								public void onTranscodeProgress(double progress) {
									progressDialog.setProgress((int) (progress * 100));
								}

								@Override
								public void onTranscodeCompleted() {
									Log.d("Hiking", "onTranscodeCompleted");
									Toast.makeText(MainActivity.this, "Transcode completed.",
											Toast.LENGTH_LONG).show();
									closeDescriptorSilently();
									progressDialog.dismiss();
								}

								@Override
								public void onTranscodeCanceled() {
									Log.d("Hiking", "onTranscodeCanceled");
									Toast.makeText(MainActivity.this, "Transcode canceled.",
											Toast.LENGTH_LONG).show();
									closeDescriptorSilently();
									progressDialog.dismiss();
								}

								@Override
								public void onTranscodeFailed(Exception exception) {
									Log.w("Hiking", "onTranscodeFailed: " + exception.getMessage());
									Toast.makeText(MainActivity.this,
											"Transcode failed: " + exception.getMessage(),
											Toast.LENGTH_LONG).show();
									exception.printStackTrace();
									closeDescriptorSilently();
									progressDialog.dismiss();
								}

								private void closeDescriptorSilently() {
									try {
										parcelFileDescriptor.close();
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void trim(final Uri videoUri) {
		String filePath = FileUtils.getPath(this, videoUri);
		TrimmerActivity.startForResult(MainActivity.this, REQUEST_VIDEO_TRIM_K4L, filePath);
	}
}
