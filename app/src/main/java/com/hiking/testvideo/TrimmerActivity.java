package com.hiking.testvideo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnK4LVideoListener;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;

public class TrimmerActivity extends AppCompatActivity
		implements OnTrimVideoListener, OnK4LVideoListener {

	private static String EXTRA_VIDEO_URI = "EXTRA_VIDEO_URI";

	public static void startForResult(Activity activity, int requestCode, String videoPath) {
		Intent starter = new Intent(activity, TrimmerActivity.class);
		starter.putExtra(EXTRA_VIDEO_URI, videoPath);
		activity.startActivityForResult(starter, requestCode);
	}

	private K4LVideoTrimmer mVideoTrimmer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trimmer);

		mVideoTrimmer = findViewById(R.id.timeLine);
		if (mVideoTrimmer != null) {
			mVideoTrimmer.setMaxDuration(10);
			mVideoTrimmer.setOnTrimVideoListener(this);
			mVideoTrimmer.setOnK4LVideoListener(this);
			mVideoTrimmer.setDestinationPath("/storage/emulated/0/Videos/Dcard/");
			mVideoTrimmer.setVideoURI(Uri.parse(getIntent().getStringExtra(EXTRA_VIDEO_URI)));
		}
	}

	@Override
	public void onTrimStarted() {
		Log.d("Hiking", "onTrimStarted");
	}

	@Override
	public void getResult(final Uri uri) {
		Log.d("Hiking", "getResult " + uri);
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(TrimmerActivity.this, "Video saved at " + uri.getPath(),
						Toast.LENGTH_SHORT).show();
			}
		});
		Intent data = new Intent();
		data.setDataAndType(uri, "video/mp4");
		setResult(RESULT_OK, data);
		finish();
	}

	@Override
	public void cancelAction() {
		Log.d("Hiking", "cancelAction ");
		mVideoTrimmer.destroy();
		finish();
	}

	@Override
	public void onError(final String message) {
		Log.d("Hiking", "onError " + message);
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(TrimmerActivity.this, "onError: " + message, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	@Override
	public void onVideoPrepared() {
		Log.d("Hiking", "onVideoPrepared");
	}
}