package com.hiking.testvideo;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class VideoChooserActivity extends AppCompatActivity {

	private List<VideoInfo> mVideoInfos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_chooser);

		long time = System.currentTimeMillis();
		mVideoInfos = getAllVideoPath(this);
		Log.d("Hiking",
				"Size = " + mVideoInfos.size() + ", takes " + (System.currentTimeMillis() - time));

		RecyclerView recyclerView = findViewById(R.id.recycler);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		recyclerView.setAdapter(new Adapter());
	}

	private class VideoInfo {

		String path;
		long duration;
	}

	private List<VideoInfo> getAllVideoPath(Context context) {
		// Retrieve video info
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();

		Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
		String[] projection = {MediaStore.Video.VideoColumns.DATA};
		String orderBy = android.provider.MediaStore.Video.Media.DATE_TAKEN;
		Cursor cursor =
				context.getContentResolver().query(uri, projection, null, null, orderBy + " DESC");
		List<VideoInfo> pathArrList = new ArrayList<>();
		if (cursor != null) {
			while (cursor.moveToNext()) {
				String path = cursor.getString(0);
				retriever.setDataSource(context, Uri.parse(path));
				VideoInfo info = new VideoInfo();
				info.path = path;
				info.duration = Long.parseLong(
						retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
				pathArrList.add(info);
			}
			cursor.close();
		}

		retriever.release();
		return pathArrList;
	}

	private class Adapter extends RecyclerView.Adapter {

		class ViewHolder extends RecyclerView.ViewHolder {

			TextView textView1, textView2;

			ViewHolder(View itemView) {
				super(itemView);
				this.textView1 = itemView.findViewById(android.R.id.text1);
				this.textView2 = itemView.findViewById(android.R.id.text2);
			}
		}

		@Override
		public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new ViewHolder(LayoutInflater.from(parent.getContext())
					.inflate(android.R.layout.simple_list_item_2, parent, false));
		}

		@Override
		public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
			VideoInfo info = mVideoInfos.get(position);
			ViewHolder viewHolder = (ViewHolder) holder;
			viewHolder.textView1.setText(info.path);
			viewHolder.textView2.setText(String.valueOf(info.duration));
		}

		@Override
		public int getItemCount() {
			return mVideoInfos == null ? 0 : mVideoInfos.size();
		}
	}
}
